extern crate num_cpus;
extern crate petgraph;
extern crate structopt;
#[macro_use]
extern crate log;
extern crate env_logger;
extern crate rand;
extern crate threadpool;

pub mod activities;
pub mod ant;
pub mod colony;
pub mod cycle;
pub mod io;

#[derive(Clone, Debug)]
pub enum Objective {
    /// Visit all of the nodes of the graph (will fail on an incomplete graph)
    VisitAllNodes,
    /// Visit exactly this many nodes of the graph
    VisitNNodes(usize),
    /// Visit node indexed by the provided index
    ReachNodeIdx(usize),
    /// Reach a dead end, this can be useful for going as far down as possible
    ReachDeadEnd,
}
