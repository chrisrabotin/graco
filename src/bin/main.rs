extern crate env_logger;
extern crate graco;
extern crate num_cpus;
extern crate petgraph;
extern crate rand;
extern crate structopt;
extern crate threadpool;
#[macro_use]
extern crate log;

use graco::colony::{Colony, ColonyOpts};
use num_cpus::get as get_num_cpus;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, Clone, StructOpt)]
#[structopt(
    name = "graco",
    about = "Generalized Rust Min-Max Ant Colony Optimization"
)]
pub struct CmdOpt {
    /// Verbose mode will output all pheromone graphs at each iteration.
    #[structopt(short = "v", long = "verbose")]
    verbose: bool,
    /// Set the initial pheromone in the graph
    #[structopt(short = "p", long = "initphe", default_value = "1.0")]
    initphe: f64,
    /// Set the evaporation rate, between 0 and 1, lower faster: 0 is iteration-only pheromones.
    #[structopt(short = "e", long = "evap", default_value = "0.95")]
    evaporation: f64,
    /// Set the maximum number of iterations
    #[structopt(short = "i", long = "iterations", default_value = "100")]
    iterations: u32,
    /// Set the percentage of ants which need to find the same path to consider the problem solved
    #[structopt(short = "t", long = "threshold", default_value = "1.0")]
    threshold: f64,
    /// Density constant Q of the pheromones
    #[structopt(short = "d", long = "density", default_value = "1")]
    density: f64,
    /// Set the relative importance of pheromones for the probabilistic function
    #[structopt(long = "alpha", default_value = "1")]
    alpha: i32,
    /// Set the relative importance of distance for the probabilistic function
    #[structopt(long = "beta", default_value = "2")]
    beta: i32,
    /// Set the probability of not following the pheromone trail
    #[structopt(long = "wander", default_value = "0.15")]
    wander: f64,
    /// Number of ants, maxed to the number of nodes in the graph
    #[structopt(long = "ants", default_value = "50")]
    ants: usize,
    /// Change the number of CPUs to run on (default is max available)
    #[structopt(long = "cpus", default_value = "0")]
    cpus: usize,
    /// Set the seed of the first ant. Each subsequent ant will have that number incremented
    #[structopt(long = "seed")]
    seed: Option<u64>,
    /// Input path to the graph text file
    #[structopt(parse(from_os_str))]
    input: PathBuf,
    /// By default, the objective is to visit all nodes
    #[structopt(long = "obj-all", group = "obj")]
    visit_all: bool,
    /// Specify the name of the node to reach
    #[structopt(long = "obj-node", group = "obj")]
    visit_node: Option<String>,
    /// Specify the number of nodes to reach
    #[structopt(long = "obj-count", group = "obj")]
    visit_count: Option<usize>,
    /// Specify the number of nodes to reach
    #[structopt(long = "obj-deadend", group = "obj")]
    reach_dead_end: bool,
    /// Specify the starting node name
    #[structopt(short = "s", long = "start")]
    start_node: Option<String>,
}

fn main() {
    env_logger::init();
    let mut opt = CmdOpt::from_args();

    let num_cpus = get_num_cpus();
    if opt.cpus == 0 || opt.cpus > num_cpus {
        opt.cpus = num_cpus;
        info!("Running on {} CPUs", opt.cpus);
    }

    let colony_opt = ColonyOpts {
        verbose: opt.verbose,
        initphe: opt.initphe,
        evaporation: opt.evaporation,
        iterations: opt.iterations,
        threshold: opt.threshold,
        density: opt.density,
        alpha: opt.alpha,
        beta: opt.beta,
        seed: opt.seed,
        visit_node: opt.visit_node,
        visit_count: opt.visit_count,
        reach_dead_end: opt.reach_dead_end,
        start_node: opt.start_node,
        wander: opt.wander,
        ants: opt.ants,
        cpus: opt.cpus,
    };

    let mut colony = Colony::with_input_path(colony_opt, opt.input);
    colony.explore();
}
